package net.ridho.cm;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.pixplicity.easyprefs.library.Prefs;

import net.ridho.cm.apiClient.ServiceGenerator;
import net.ridho.cm.apiClient.iCourseService;
import net.ridho.cm.model.CMResponse;
import net.ridho.cm.model.Course;
import net.ridho.cm.model.UserCourse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

    private String accessToken = "";
    ArrayAdapter<String> adapter;
    ArrayList<String> enrolledCoursesNames;
    private String userId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        enrolledCoursesNames = new ArrayList<>();

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                enrolledCoursesNames);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.mainToolbarTitle));
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ListView listView = (ListView) findViewById(R.id.enrolledCourseListView);
        listView.setAdapter(adapter);

        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();

        accessToken = Prefs.getString("accessToken", "");
        userId = Prefs.getString("userId", "");
        if (accessToken != "") {
            UserController.getInstance().setLoggedIn(true, accessToken);

            // todo: check if token is valid
        } else {
            getEnrolledCourses();
        }

        if (!UserController.getInstance().isLoggedIn()) {
            Intent intent = new Intent(this, LoginButton.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (accessToken != "") {
            getEnrolledCourses();
        }
    }

    private void getEnrolledCourses() {
        iCourseService taskService = ServiceGenerator.createService(iCourseService.class, accessToken);
        Call<CMResponse.CourseResponse> call = taskService.getEnrolledCourses(userId);
        call.enqueue(new Callback<CMResponse.CourseResponse>() {
            @Override
            public void onResponse(Response<CMResponse.CourseResponse> response, Retrofit retrofit) {
                if (response.isSuccess()) {
                    // courses available
                    CMResponse.CourseResponse resp = response.body();
                    printCourses(resp.courses);
                } else {
                    // error response, no access to resource?
                    try {
                        Log.d("Error", response.errorBody().string());
                        System.out.println(response.errorBody().string());
                        String rw = response.raw().body().string();
                        System.out.println(rw);
                    } catch (IOException e) {
                    }
                }
            }

            @Override
            public void onFailure(Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
            }
        });
    }

    private void printCourses(List<UserCourse> courses) {
        for (int i = 0; i < courses.size(); i++) {
            enrolledCoursesNames.add(courses.get(i).course.name);
        }

        adapter.notifyDataSetChanged();
    }
}
