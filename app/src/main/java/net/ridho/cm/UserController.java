package net.ridho.cm;

/**
 * Created by ridho on 2/01/16.
 */
public class UserController {

    private static UserController ourInstance = new UserController();
    private String accessToken;
    private String userId;

    public static UserController getInstance() {
        return ourInstance;
    }

    private boolean isLoggedInStatus = false;

    public boolean isLoggedIn() {
        return isLoggedInStatus;
    }

    public void setLoggedIn(boolean isL) {
        isLoggedInStatus = isL;
    }

    public void setLoggedIn(boolean isLoggedIn, String acc) {
        this.accessToken = acc;
        this.isLoggedInStatus = isLoggedIn;
    }

    public void setAccessToken(String acc) {
        this.accessToken = acc;
    }

    private UserController() {
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
