package net.ridho.cm.model;

import java.util.Date;

/**
 * Created by ridho on 7/01/16.
 */
public class UserCourse {
    /**
     * userId _id in CM
     */
    public String user;
    public Course course;
    public boolean isEnrolled;
}
