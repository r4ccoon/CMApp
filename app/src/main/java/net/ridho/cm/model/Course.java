package net.ridho.cm.model;

public class Course {
    public String name;
    public String slug;
    public String description;
    public String smallDescription;
    public String picture;
    public String video;
}