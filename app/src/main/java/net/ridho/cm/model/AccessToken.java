package net.ridho.cm.model;

import net.ridho.cm.apiClient.CMToken;

/**
 * Created by ridho on 3/01/16.
 */
public class AccessToken {
    public String token_type;
    public CMToken access_token;
}
