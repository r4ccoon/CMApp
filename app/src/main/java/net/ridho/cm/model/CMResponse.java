package net.ridho.cm.model;

import java.util.List;

/**
 * Created by ridho on 7/01/16.
 */
public class CMResponse {
    public class CourseResponse {
        public boolean result;
        public List<UserCourse> courses;
    }
}
