package net.ridho.cm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.content.Intent;

public class LoginButton extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_button);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (UserController.getInstance().isLoggedIn()) {
            finish();
        }
    }

    public void openLoginWebView(View view) {
        Intent intent = new Intent(this, LoginWebView.class);
        startActivity(intent);
    }

}
