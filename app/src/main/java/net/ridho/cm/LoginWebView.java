package net.ridho.cm;

import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pixplicity.easyprefs.library.Prefs;

import net.ridho.cm.model.AccessToken;
import net.ridho.cm.apiClient.GetTokenAsync;
import net.ridho.cm.apiClient.ServiceGenerator;
import net.ridho.cm.apiClient.iAsyncResponse;
import net.ridho.cm.apiClient.iLoginService;

import retrofit.Call;

public class LoginWebView extends AppCompatActivity {

    private WebView webView;
    private String loginUrl;
    private String exchangeUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loginUrl = String.format(getString(R.string.baseUrl) + getString(R.string.loginUrl), getString(R.string.clientId));
        exchangeUrl = getString(R.string.baseUrl) + getString(R.string.exchangeUrl);

        setContentView(R.layout.activity_login_webview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.loginToolbarTitle));
        setSupportActionBar(toolbar);

        webView = (WebView) findViewById(R.id.webView2);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                if (url.contains("/api/oauth2/app?code=")) {
                    exchangeToken(url);
                } else {
                    // show failed
                }
            }
        });

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(loginUrl);
    }

    protected void exchangeToken(String url) {
        String[] lk = url.split("=");
        String code = lk[1];

        iLoginService loginService =
                ServiceGenerator.createService(iLoginService.class, getString(R.string.clientId), getString(R.string.clientSecret));

        /*grant_type=authorization_code
        code=hIcLEqx4nu5SeS8i
        redirectUri=*/

        Call<AccessToken> tokenCall = loginService.getToken(code, "authorization_code");
        new GetTokenAsync(new iAsyncResponse() {
            @Override
            public void processFinish(Object output) {
                retrofit.Response<AccessToken> response = (retrofit.Response<AccessToken>) output;
                if (output != null && response.body() != null) {
                    AccessToken bod = response.body();
                    String acc = bod.access_token.token;
                    String userId = bod.access_token.userId;
                    if (acc != null) {
                        Prefs.putString("accessToken", acc);
                        Prefs.putString("userId", userId);

                        // go to main screen.
                        UserController.getInstance().setLoggedIn(true, acc);
                        UserController.getInstance().setUserId(userId);
                        onLoggedIn();
                    }
                }
            }
        }).execute(tokenCall);
    }

    public void onLoggedIn() {
        finish();
    }
}
