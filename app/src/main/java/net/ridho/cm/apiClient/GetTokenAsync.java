package net.ridho.cm.apiClient;

import android.os.AsyncTask;
import android.util.Log;

import net.ridho.cm.model.AccessToken;

import java.io.IOException;

import retrofit.Call;

public class GetTokenAsync extends AsyncTask<Call<AccessToken>, Void, retrofit.Response<AccessToken>> {

    public iAsyncResponse delegate = null;

    public GetTokenAsync(iAsyncResponse asyncResponse) {
        delegate = asyncResponse;
    }

    @Override
    protected retrofit.Response<AccessToken> doInBackground(Call<AccessToken>... calls) {
        try {
            retrofit.Response<AccessToken> token = calls[0].execute();

            return token;
        } catch (IOException e) {
            Log.v("token", e.getMessage());
        }

        return null;
    }

    protected void onPostExecute(retrofit.Response<AccessToken> token) {
        if (token != null) {
            Log.d("token", token.toString());
        }

        delegate.processFinish(token);
    }

}
