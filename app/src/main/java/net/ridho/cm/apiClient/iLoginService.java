package net.ridho.cm.apiClient;

import net.ridho.cm.UserController;
import net.ridho.cm.model.AccessToken;

import retrofit.Call;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Field;

/**
 * Created by ridho on 3/01/16.
 */
public interface iLoginService {
    @GET("/api/account")
    Call<UserController> getUser();

    @POST("/api/oauth2/token")
    @FormUrlEncoded
    Call<AccessToken> getToken(
            @Field("code") String code,
            @Field("grant_type") String grant_type
    );
}
