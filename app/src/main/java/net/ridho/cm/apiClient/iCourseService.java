package net.ridho.cm.apiClient;

import net.ridho.cm.model.CMResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by ridho on 7/01/16.
 */
public interface iCourseService {
    @GET("/api/account/{userId}/courses")
    Call<CMResponse.CourseResponse> getEnrolledCourses(@Path("userId") String userId);
}
