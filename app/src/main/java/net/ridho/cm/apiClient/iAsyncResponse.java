package net.ridho.cm.apiClient;

/**
 * Created by ridho on 3/01/16.
 */
public interface iAsyncResponse {
    void processFinish(Object output);
}
